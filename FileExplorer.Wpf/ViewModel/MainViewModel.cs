﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using FileExplorer.Wpf.Model;
using GalaSoft.MvvmLight.Command;

namespace FileExplorer.Wpf.ViewModel
{
    class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            _instance = this;

            ExtendedLoadCommand = new RelayCommand(ExtendedLoadMethod);
            LoadPreviousCommand = new RelayCommand(LoadPreviousMethod);
            TreeviewSelectedItemChanged = new RelayCommand<FolderElement>(LoadFromTreeMethod);

            CopyCommand = new RelayCommand(CopyMethod);
            CutCommand = new RelayCommand(CutMethod);
            PasteCommand = new RelayCommand(PasteMethod);

            _drive = new ObservableCollection<FolderElement>();

            FillTreeMethod();
        }

        Commands _commands;

        private static MainViewModel _instance;

        private ObservableCollection<FolderElement> _file;
        private ObservableCollection<FolderElement> _drive;

        private FolderElement _selectedElement;
        private string _currentFolderPath;

        private bool _copyState;
        private bool _cutState;

        public RelayCommand ExtendedLoadCommand { get; set; }
        public RelayCommand LoadPreviousCommand { get; set; }
        public RelayCommand<FolderElement> TreeviewSelectedItemChanged { get; set; }
        public RelayCommand CopyCommand { get; set; }
        public RelayCommand CutCommand { get; set; }
        public RelayCommand PasteCommand { get; set; }

        public ObservableCollection<FolderElement> File => _file ?? (_file = new ObservableCollection<FolderElement>());
        public ObservableCollection<FolderElement> _File
        {
            get => _file;
            set
            {
                _file = value;
                OnPropertyChanged($"File");
            }
        }

        public ObservableCollection<FolderElement> Drive
        {
            get => _drive;
            set => _drive = value;
        }

        public string PreviousFolder
        {
            get
            {
                if (_currentFolderPath.Split('\\').Count() > 1)
                {
                    var result = from sub in _currentFolderPath.Split('\\')
                                 where sub != _currentFolderPath.Split('\\').Last()
                                 select sub;
                    var enumerable = result as string[] ?? result.ToArray();
                    if (enumerable.Count() == 1)
                    {
                        return String.Join("\\", enumerable) + "\\";
                    }
                    return String.Join("\\", enumerable);
                }
                else
                    return _currentFolderPath = _currentFolderPath + '\\';
            }
        }
        
        public FolderElement SelectedFile
        {
            set
            {
                _selectedElement = value;
                OnPropertyChanged();
            }
        }

        public string CurrentFolder
        {
            get => _currentFolderPath;
            set
            {
                _currentFolderPath = value;
                OnPropertyChanged();
            }
        }
               
        public static MainViewModel GetInstance()
        {
            return _instance;
        }

 
        private void ExtendedLoadMethod()
        {
            try
            {
                if (_selectedElement.Type == ElementType.Folder)
                {
                    string folderPath = _selectedElement.FullName;
                    var folderInf = new FolderData(folderPath);
                    _file = new ObservableCollection<FolderElement>(folderInf.GetData());
                    CurrentFolder = folderPath;
                    OnPropertyChanged(nameof(File));
                    return;
                }

                if (_selectedElement.Type == ElementType.File)
                {
                    string folderPath = _selectedElement.FullName;
                    System.Diagnostics.Process.Start(folderPath);
                }
            }

            catch
            {
                // ignored
            }
        }

        private void LoadPreviousMethod()
        {
            try
            {
                string folderPath = PreviousFolder;
                var folderInf = new FolderData(folderPath);
                _file = new ObservableCollection<FolderElement>(folderInf.GetData());
                CurrentFolder = folderPath;
                OnPropertyChanged(nameof(File));
            }

            catch
            {
                // ignored
            }
        }

        private void FillTreeMethod()
        {
            string[] drives = Environment.GetLogicalDrives();
            foreach (string d in drives)
            {
                Drive.Add(new FolderElement { Name = d, Type = ElementType.Folder });
            }
        }

        private void LoadFromTreeMethod(FolderElement folder)
        {
            var folderInf = new FolderData(folder.FullName);
            _file = new ObservableCollection<FolderElement>(folderInf.GetData());
            CurrentFolder = folder.FullName;
            OnPropertyChanged(nameof(File));
        }

        private void CopyMethod()
        {
            try
            {
                Mediator.SourceFile = _selectedElement;
                _copyState = true;
                _cutState = false;
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("CopyMethod");
            }
        }

        private void CutMethod()
        {
            try
            {
                Mediator.SourceFile = _selectedElement;
                _copyState = false;
                _cutState = true;
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("CutMethod");
            }
        }

        private void PasteMethod()
        {

            if (_copyState)
            {
                try
                {
                    Mediator.DestionationFile = _currentFolderPath;
                    Mediator.Type = ActionType.Copy;
                    _commands = new Commands();
                    _commands.Paste();
                    _copyState = false;

                }
                catch (Exception)
                {
                    _copyState = false;
                }
            }

            if (_cutState)
            {
                try
                {
                    Mediator.DestionationFile = _currentFolderPath;
                    Mediator.Type = ActionType.Cut;
                    _commands = new Commands();
                    _commands.Paste();
                    _cutState = false;

                }
                catch (Exception)
                {
                    _cutState = false;
                }
            }
        }

        public void AddToCollection(FolderElement folderElement)
        {
            File.Add(folderElement);
        }        
        
        public void RegexSearch(Regex regex, string destinationPath)
        {
            DirectoryInfo directory = new DirectoryInfo(destinationPath);
            DirectoryInfo[] dirs = directory.GetDirectories();
            FileInfo[] files = directory.GetFiles();

            foreach (FileInfo file in files)
            {
                if (regex.IsMatch(file.Name))
                {
                    File.Add(new FolderElement { Name = file.FullName, Type = ElementType.File });
                }
            }

            foreach (DirectoryInfo dir in dirs)
            {
                try
                {
                     RegexSearch(regex, dir.FullName);
                }
                catch (Exception)
                {

                    continue;
                }
                if (regex.IsMatch(dir.Name))
                {
                    File.Add(new FolderElement { Name = dir.FullName, Type = ElementType.Folder });
                }
            }

            CurrentFolder = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
