﻿using FileExplorer.Core.ViewModel;
using FileExplorer.Wpf.Model;

namespace FileExplorer.Wpf.ViewModel
{
    public class Folder
    {
        DataContext db;
        Directorys directory = new Directorys();

        public void FolderCopy(string from, string parentDirName, string to)
        {
            directory.DirectoryCopy(from, parentDirName, to);
            db.SaveChanges();

            MainViewModel.GetInstance().AddToCollection(new FolderElement { FullName = to, Type = ElementType.Folder });
        }

        public void FolderCut(string from, string parentDirName, string to)
        {
            directory.DirectoryCut(from, parentDirName, to);
            db.SaveChanges();

            MainViewModel.GetInstance().AddToCollection(new FolderElement { FullName = to, Type = ElementType.Folder });
        }
    }
}
