﻿using FileExplorer.Wpf.Model;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace FileExplorer.Wpf.ViewModel
{
    public class Commands
    {

        private FolderElement source = Mediator.SourceFile;
        private string destination = Mediator.DestionationFile;

        public Commands()
        {
            PasteCommand = new RelayCommand(Paste);

        }
        public RelayCommand PasteCommand { get; set; }

        public void Paste()
        {
            string fileName = source.FullName;
            string endDirectory = destination + "\\" + source.Name;

            if (Mediator.Type == ActionType.Copy)
            {
                if (source.Type == ElementType.Folder)
                {
                    try
                    {
                        FolderCopy(Mediator.SourceFile.FullName, Mediator.SourceFile.Name, endDirectory);
                    }
                    catch (TaskCanceledException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                    try
                    {
                        CopyFiles(new Dictionary<string, string> { { fileName, endDirectory } });
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
            }
            if (Mediator.Type == ActionType.Cut)
            {

                if (source.Type == ElementType.Folder)
                    try
                    {
                        FolderCut(Mediator.SourceFile.FullName, Mediator.SourceFile.Name, endDirectory);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                else
                    try
                    {
                        CutFiles(new Dictionary<string, string> { { fileName, endDirectory } });
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
            }
        }

        public void CopyFiles(Dictionary<string, string> files)
        {
            for (var x = 0; x < files.Count; x++)
            {
                var item = files.ElementAt(x);
                var from = item.Key;
                var to = item.Value;

                using (var outStream = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    using (var inStream = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        inStream.CopyTo(outStream, 128);
                    }
                }

                MainViewModel.GetInstance().AddToCollection(new FolderElement { FullName = to, Type = ElementType.File });
            }
        }

        public void CutFiles(Dictionary<string, string> files)
        {
            for (var x = 0; x < files.Count; x++)
            {
                var item = files.ElementAt(x);
                var from = item.Key;
                var to = item.Value;

                using (var outStream = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    using (var inStream = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        inStream.CopyTo(outStream, 128);
                    }
                }
                File.Delete(from);

                MainViewModel.GetInstance().AddToCollection(new FolderElement { FullName = to, Type = ElementType.File });
            }
        }

        public void FolderCopy(string from, string parentDirName, string to)
        {
            DirectoryCopy(from, parentDirName, to);

            MainViewModel.GetInstance().AddToCollection(new FolderElement { FullName = to, Type = ElementType.Folder });
        }

        public void FolderCut(string from, string parentDirName, string to)
        {
            DirectoryCut(from, parentDirName, to);

            MainViewModel.GetInstance().AddToCollection(new FolderElement { FullName = to, Type = ElementType.Folder });
        }

        public void DirectoryCopy(string from, string parentDirName, string to)
        {
            DirectoryInfo directory = new DirectoryInfo(from);

            DirectoryInfo[] dirs = directory.GetDirectories();
            FileInfo[] files = directory.GetFiles();

            if (!Directory.Exists(to + parentDirName))
            {
                Directory.CreateDirectory(to);
            }

            foreach (FileInfo file in files)
            {
                using (var outStream = new FileStream(Path.Combine(to, file.Name), FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    using (var inStream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        inStream.CopyTo(outStream, 128);
                    }
                }
            }

            foreach (DirectoryInfo subDir in dirs)
            {
                string newPath = Path.Combine(to, subDir.Name);
                DirectoryCopy(subDir.FullName, subDir.Name, newPath);
            }
        }

        public void DirectoryCut(string from, string parentDirName, string to)
        {
            DirectoryInfo directory = new DirectoryInfo(from);

            DirectoryInfo[] dirs = directory.GetDirectories();
            FileInfo[] files = directory.GetFiles();

            if (!Directory.Exists(to + parentDirName))
            {
                Directory.CreateDirectory(to);
            }

            foreach (FileInfo file in files)
            {
                using (var outStream = new FileStream(Path.Combine(to, file.Name), FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    using (var inStream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        inStream.CopyTo(outStream, 128);
                    }
                }
            }
            foreach (DirectoryInfo subDir in dirs)
            {
                string newPath = Path.Combine(to, subDir.Name);
                DirectoryCut(subDir.FullName, subDir.Name, newPath);
            }
        }
    }
}
