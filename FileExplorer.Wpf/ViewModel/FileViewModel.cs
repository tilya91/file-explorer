﻿using FileExplorer.Wpf.Model;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;

namespace FileExplorer.Wpf.ViewModel
{
    class FileViewModel : INotifyPropertyChanged
    {

        private static FileViewModel instance;

        private ObservableCollection<FolderElement> _file;
        private ObservableCollection<FolderElement> _drive;

        private FolderElement selectedElement;
        private string currentFolderPath;

        private bool copyState = false;
        private bool cutState = false;


        public ObservableCollection<FolderElement> File => _file ?? (_file = new ObservableCollection<FolderElement>());
        public ObservableCollection<FolderElement> _File
        {
            get => _file;
            set
            {
                _file = value;
                OnPropertyChanged("File");
            }
        }


        public ObservableCollection<FolderElement> Drive
        {
            get => _drive;
            set => _drive = value;
        }

        public string PreviousFolder
        {
            get
            {
                if (currentFolderPath.Split('\\').Count() > 1)
                {
                    var result = from sub in currentFolderPath.Split('\\')
                                 where sub != currentFolderPath.Split('\\').Last()
                                 select sub;
                    if (result.Count() == 1)
                    {
                        return String.Join("\\", result) + "\\";
                    }
                    return String.Join("\\", result);
                }
                else
                    return currentFolderPath = currentFolderPath + '\\';
            }
        }
        public FolderElement SelectedFile
        {
            set
            {
                selectedElement = value;
                OnPropertyChanged("SelectedFile");
            }
        }

        public string CurrentFolder
        {
            get => currentFolderPath;
            set
            {
                currentFolderPath = value;
                OnPropertyChanged("CurrentFolder");
            }
        }


        public RelayCommand ExtendedLoadCommand { get; set; }
        public RelayCommand LoadPreviousCommand { get; set; }
        public RelayCommand<FolderElement> TreeviewSelectedItemChanged { get; set; }
        public RelayCommand CopyCommand { get; set; }
        public RelayCommand CutCommand { get; set; }
        public RelayCommand PasteCommand { get; set; }

        public FileViewModel()
        {
            instance = this;

            ExtendedLoadCommand = new RelayCommand(ExtendedLoadMethod);
            LoadPreviousCommand = new RelayCommand(LoadPreviousMethod);
            TreeviewSelectedItemChanged = new RelayCommand<FolderElement>(LoadFromTreeMethod);

            CopyCommand = new RelayCommand(CopyMethod);
            CutCommand = new RelayCommand(CutMethod);
            PasteCommand = new RelayCommand(PasteMethod);

            _drive = new ObservableCollection<FolderElement>();

            FillTreeMethod();
        }

        public static FileViewModel GetInstance()
        {
            return instance;
        }


        private void ExtendedLoadMethod()
        {
            try
            {
                if (selectedElement.Type == ElementType.Folder)
                {
                    string folderPath = selectedElement.FullName;
                    var folderInf = new FolderData(folderPath);
                    _file = new ObservableCollection<FolderElement>(folderInf.GetData());
                    CurrentFolder = folderPath;
                    OnPropertyChanged(nameof(File));
                    return;
                }

                if (selectedElement.Type == ElementType.File)
                {
                    string folderPath = selectedElement.FullName;
                    System.Diagnostics.Process.Start(folderPath);
                    return;
                }
            }

            catch
            {
                return;
            }
        }

        private void LoadPreviousMethod()
        {
            try
            {
                string folderPath = PreviousFolder;
                var folderInf = new FolderData(folderPath);
                _file = new ObservableCollection<FolderElement>(folderInf.GetData());
                CurrentFolder = folderPath;
                OnPropertyChanged(nameof(File));
            }

            catch
            {

            }

        }

        private void FillTreeMethod()
        {
            string[] drives = Environment.GetLogicalDrives();
            foreach (string d in drives)
            {
                Drive.Add(new FolderElement { Name = d, Type = ElementType.Folder });
            }
        }

        private void LoadFromTreeMethod(FolderElement folder)
        {
            var folderInf = new FolderData(folder.FullName);
            _file = new ObservableCollection<FolderElement>(folderInf.GetData());
            CurrentFolder = folder.FullName;
            OnPropertyChanged(nameof(File));
        }

        private void CopyMethod()
        {
            try
            {
                Mediator.SourceFile = selectedElement;
                copyState = true;
                cutState = false;
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CutMethod()
        {
            try
            {
                Mediator.SourceFile = selectedElement;
                copyState = false;
                cutState = true;
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PasteMethod()
        {
            if (copyState)
            {
                try
                {
                    Mediator.DestionationFile = currentFolderPath;
                    Mediator.Type = ActionType.Copy;
                    Commands commands = new Commands();
                    commands.Paste();
                    copyState = false;

                }
                catch (Exception)
                {
                    copyState = false;
                }
            }

            if (cutState)
            {
                try
                {
                    Mediator.DestionationFile = currentFolderPath;
                    Mediator.Type = ActionType.Cut;
                    Commands commands = new Commands();
                    commands.Paste();
                    cutState = false;

                }
                catch (Exception)
                {
                    cutState = false;
                }
            }
        }


        public void AddToCollection(FolderElement folderElement)
        {
            File.Add(folderElement);
        }

        public void RegexSearch(Regex regex, string destinationPath)
        {
            DirectoryInfo directory = new DirectoryInfo(destinationPath);
            DirectoryInfo[] dirs = directory.GetDirectories();
            FileInfo[] files = directory.GetFiles();

            foreach (FileInfo file in files)
            {
                if (regex.IsMatch(file.Name))
                {
                    File.Add(new FolderElement { Name = file.FullName, Type = ElementType.File });
                }
            }

            foreach (DirectoryInfo dir in dirs)
            {
                try
                {
                    RegexSearch(regex, dir.FullName);
                }
                catch (Exception)
                {

                    continue;
                }
                if (regex.IsMatch(dir.Name))
                {
                    File.Add(new FolderElement { Name = dir.FullName, Type = ElementType.Folder });
                }
            }

            CurrentFolder = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
