﻿using FileExplorer.Core.ViewModel;
using FileExplorer.Wpf.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileExplorer.Wpf.ViewModel
{
    public class Files
    {
        DataContext db;

        public void CopyFiles(Dictionary<string, string> files)
        {
            for (var x = 0; x < files.Count; x++)
            {
                var item = files.ElementAt(x);
                var from = item.Key;
                var to = item.Value;

                using (var outStream = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    using (var inStream = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        inStream.CopyTo(outStream, 128);
                    }
                }
                db.SaveChanges();

                MainViewModel.GetInstance().AddToCollection(new FolderElement { FullName = to, Type = ElementType.File });
            }
        }

        public void CutFiles(Dictionary<string, string> files)
        {
            for (var x = 0; x < files.Count; x++)
            {
                var item = files.ElementAt(x);
                var from = item.Key;
                var to = item.Value;

                using (var outStream = new FileStream(to, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    using (var inStream = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        inStream.CopyTo(outStream, 128);
                    }
                }
                File.Delete(from);
                db.SaveChanges();

                MainViewModel.GetInstance().AddToCollection(new FolderElement { FullName = to, Type = ElementType.File });
            }
        }
    }
}
