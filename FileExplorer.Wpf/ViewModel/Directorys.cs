﻿using FileExplorer.Wpf.Model;
using System.IO;

namespace FileExplorer.Wpf.ViewModel
{
    public class Directorys
    {
        DataContext db;

        public void DirectoryCopy(string from, string parentDirName, string to)
        {
            DirectoryInfo directory = new DirectoryInfo(from);

            DirectoryInfo[] dirs = directory.GetDirectories();
            FileInfo[] files = directory.GetFiles();

            if (!Directory.Exists(to + parentDirName))
            {
                Directory.CreateDirectory(to);
            }

            foreach (FileInfo file in files)
            {
                using (var outStream = new FileStream(Path.Combine(to, file.Name), FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    using (var inStream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        inStream.CopyTo(outStream, 128);
                    }
                }
            }

            foreach (DirectoryInfo subDir in dirs)
            {
                string newPath = Path.Combine(to, subDir.Name);
                DirectoryCopy(subDir.FullName, subDir.Name, newPath);
            }
        }

        public void DirectoryCut(string from, string parentDirName, string to)
        {
            DirectoryInfo directory = new DirectoryInfo(from);

            DirectoryInfo[] dirs = directory.GetDirectories();
            FileInfo[] files = directory.GetFiles();

            if (!Directory.Exists(to + parentDirName))
            {
                Directory.CreateDirectory(to);
            }

            foreach (FileInfo file in files)
            {
                using (var outStream = new FileStream(Path.Combine(to, file.Name), FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    using (var inStream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        inStream.CopyTo(outStream, 128);
                    }
                }
            }
            foreach (DirectoryInfo subDir in dirs)
            {
                string newPath = Path.Combine(to, subDir.Name);
                DirectoryCut(subDir.FullName, subDir.Name, newPath);
            }
        }
    }
}
