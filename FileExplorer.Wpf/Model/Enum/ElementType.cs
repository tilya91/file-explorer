﻿namespace FileExplorer.Wpf.Model
{
    public enum ElementType
    {
        Folder,
        File
    }
}
