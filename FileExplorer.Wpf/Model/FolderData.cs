﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;

namespace FileExplorer.Wpf.Model
{
    public class FolderData
    {
        private readonly string _folderpath;

        public FolderData(string folderpath)
        {
            _folderpath = folderpath;
        }

        public string FolderPath
        {
            get => _folderpath;
        }

        private IEnumerable<FolderElement> GetFiles()
        {
            return Directory.GetFiles(_folderpath).Select(x => new FolderElement { Name = x, Type = ElementType.File });
        }

        private IEnumerable<FolderElement> GetFolders()
        {
            return Directory.GetDirectories(_folderpath).Select(x => new FolderElement { Name = x, Type = ElementType.Folder });
        }

        public IEnumerable<FolderElement> GetData()
        {
            try
            {
                return GetFiles().Union(GetFolders());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return Enumerable.Empty<FolderElement>();
            }
        }
    }
}
