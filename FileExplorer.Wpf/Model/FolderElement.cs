﻿using System;
using System.Linq;
using System.Windows.Media.Imaging;

namespace FileExplorer.Wpf.Model
{

    public class FolderElement
    {
        private string _name;

        public string Name
        {
            get => _name.Split('\\').Last();
            set => _name = value;
        }

        public string Extension
        {
            get
            {
                if (this.Type == ElementType.File)
                    return _name.Split('\\', '.').Last();
                else
                    return "Folder with files";
            }
        }
        public ElementType Type { get; set; }

        public string FullName
        {
            get => _name;
            set => _name = value;
        }

        public BitmapImage Image
        {
            get
            {
                if (this.Type == ElementType.File)
                {
                    string extension = this.Extension;
                    try
                    {
                        return new BitmapImage(new Uri(Environment.CurrentDirectory + "\\png\\" + extension + ".png"));
                    }
                    catch
                    {
                        return new BitmapImage(new Uri(Environment.CurrentDirectory + "\\png\\file.png"));
                    }
                }
                else
                    return new BitmapImage(new Uri(Environment.CurrentDirectory + "\\png\\folder.ico"));
            }
        }

    }
}
