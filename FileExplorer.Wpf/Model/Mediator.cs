﻿namespace FileExplorer.Wpf.Model
{
   
    public static class Mediator
    {
        private static FolderElement _sourceFile;
        private static string _destinationFile;

        public static FolderElement SourceFile
        {
            get => _sourceFile;
            set => _sourceFile = value;
        }

        public static string DestionationFile
        {
            get => _destinationFile;
            set => _destinationFile = value;
        }

        public static ActionType Type { get; set; }
    }
}
