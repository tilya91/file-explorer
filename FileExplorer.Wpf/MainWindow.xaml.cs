﻿using System.Windows;
using FileExplorer.Wpf.ViewModel;

namespace FileExplorer.Wpf
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = new MainViewModel();

        }

    }
}
