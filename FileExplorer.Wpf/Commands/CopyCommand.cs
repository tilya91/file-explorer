﻿using System;
using System.Windows;
using FileExplorer.Core;

namespace FileExplorer.Wpf.Commands
{
    public class CopyCommand : ICopyCommand
    {
        public void Copy(string source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (!System.IO.File.Exists(source))
                throw new InvalidOperationException($"Cannot found file by path '{source}'");

            System.Collections.Specialized.StringCollection fileCollection =
                new System.Collections.Specialized.StringCollection {source};

            Clipboard.SetFileDropList(fileCollection);
        }

        public void Cut(string source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (!System.IO.File.Exists(source))
                throw new InvalidOperationException($"Cannot found file by path '{source}'");

            System.Collections.Specialized.StringCollection fileCollection =
                new System.Collections.Specialized.StringCollection { source };

            Clipboard.SetFileDropList(fileCollection);
        }

        public void Paste(string source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (!System.IO.File.Exists(source))
                throw new InvalidOperationException($"Cannot found file by path '{source}'");

            System.Collections.Specialized.StringCollection fileCollection =
                new System.Collections.Specialized.StringCollection { source };

            Clipboard.SetFileDropList(fileCollection);
        }
    }
}