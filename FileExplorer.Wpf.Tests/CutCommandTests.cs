﻿using System.IO;
using System.Windows;
using FileExplorer.Core;
using FileExplorer.Wpf.Commands;
using Xunit;
using File = FileExplorer.Core.File;

namespace FileExplorer.Wpf.Tests
{
    public class CutCommandTests
    {
        private File CreateFile()
        {
            var directory = Directory.GetCurrentDirectory();
            var fullPath = Path.Combine(directory, "test.txt");

            var file = new ConcreteFile(fullPath);
            System.IO.File.Create(file.FullPath);

            return file;
        }

        [Fact]
        public void ItShouldCutFile()
        {
            var file = CreateFile();

            var sut = new CopyCommand();

            sut.Cut(file.FullPath);

            var clipboardFile = Clipboard.GetDataObject();

        }

        [Fact]
        public void ItShouldCutFolder()
        {
            var folder = CreateFile();

            var sut = new CopyCommand();

            sut.Cut(folder.FullPath);

            var clipboardFile = Clipboard.GetDataObject();
        }
    }
}
