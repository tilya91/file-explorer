﻿using System.IO;
using System.Windows;
using FileExplorer.Core;
using FileExplorer.Wpf.Commands;
using Xunit;
using File = FileExplorer.Core.File;

namespace FileExplorer.Wpf.Tests
{ 
    public class CopyCommandTests
    { 
        private File CreateFile()
        {
            var directory = Directory.GetCurrentDirectory();
            var fullPath = Path.Combine(directory, "test.txt");

            var file = new ConcreteFile(fullPath);
            System.IO.File.Create(file.FullPath);

            return file;
        }

        [Fact]
        public void ItShouldCopyFile()
        {
            var file = CreateFile();

            var sut = new CopyCommand();

            sut.Copy(file.FullPath);

            var clipboardFile = Clipboard.GetDataObject(); // Получаешь файл

            //Assert.Equal(file.Name, clipboardFile.ToString());
        }

        [Fact]
        public void ItShouldCopyFolder()
        {
            var folder = CreateFile();

            var sut = new CopyCommand();

            sut.Copy(folder.FullPath);

            var clipboardFile = Clipboard.GetDataObject();
        }
    }
}