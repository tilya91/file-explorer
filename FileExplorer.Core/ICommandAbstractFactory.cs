﻿using JetBrains.Annotations;

namespace FileExplorer.Core
{
    public interface ICommandAbstractFactory
    {
        [NotNull]
        ICopyCommand CreateCopyCommand();

        [NotNull]
        ICutCommand CreateCutCommand();

        [NotNull]
        IPastCommand CreatePastCommand();
    }
}
