﻿using System.IO;
using JetBrains.Annotations;

namespace FileExplorer.Core
{
    public class ConcreteFile : File
    {
        [NotNull] public string Extention => Path.GetExtension(FullPath);

        public ConcreteFile([NotNull] string fullPath)
            : base(fullPath)
        {
        }
    }
}