﻿using JetBrains.Annotations;
using System.Collections.Generic;

namespace FileExplorer.Core
{
    public class Folder : File
    {
        public IList<File> Content { get; }

        public Folder([NotNull]string fullPath, [NotNull]IList<File> content)
            : base( fullPath)
        {
            Content = content;
        }
    }
}
