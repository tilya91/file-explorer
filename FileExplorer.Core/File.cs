﻿using JetBrains.Annotations;
using System;
using System.IO;

namespace FileExplorer.Core
{
    public abstract class File
    {
        public static ICommandAbstractFactory CommandAbstractFactory { get; set; }

        [NotNull] public string Name => Path.GetFileName(FullPath);

        [NotNull] public string FullPath { get; }

        protected File([NotNull] string fullPath)
        {
            FullPath = fullPath ?? throw new ArgumentNullException(nameof(fullPath));
        }

        public static void Paste([NotNull] string destination)
        {
            var pastCommand = CommandAbstractFactory.CreatePastCommand();
            pastCommand.Past(destination);
        }

        public void Copy()
        {
            var copyCommand = CommandAbstractFactory.CreateCopyCommand();
            copyCommand.Copy(FullPath);
        }

        public void Cut()
        {
            var cutCommand = CommandAbstractFactory.CreateCutCommand();
            cutCommand.Cut(FullPath);
        }
    }
}