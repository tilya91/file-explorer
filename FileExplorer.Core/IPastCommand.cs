﻿using JetBrains.Annotations;

namespace FileExplorer.Core
{
    public interface IPastCommand
    {
        void Past([NotNull] string destination);
    }
}