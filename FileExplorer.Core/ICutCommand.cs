﻿using JetBrains.Annotations;

namespace FileExplorer.Core
{
    public interface ICutCommand
    {
        void Cut([NotNull]string source);
    }
}