﻿using JetBrains.Annotations;

namespace FileExplorer.Core
{
    public interface ICopyCommand
    {
        void Copy([NotNull]string source);
    }
}
