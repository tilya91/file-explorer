﻿using Moq;
using Xunit;

namespace FileExplorer.Core.Tests
{
    public class FileTests
    {
        private readonly Mock<ICommandAbstractFactory> _commandFactoryMock;

        public FileTests()
        {
            _commandFactoryMock = new Mock<ICommandAbstractFactory>();
            File.CommandAbstractFactory = _commandFactoryMock.Object;

        }
        private File CreateSut()
        {
            return new ConcreteFile("c://test.txt");
        }
        
        [Fact]
        public void ItShouldCopy()
        {
            var copyCommandMock = new Mock<ICopyCommand>();
            _commandFactoryMock.Setup(f => f.CreateCopyCommand())
                .Returns(() => copyCommandMock.Object);

            var sut = CreateSut();
            var path = sut.FullPath;

            sut.Copy();
            copyCommandMock.Verify(d => d.Copy(path), Times.Once);
        }
        
        [Fact]
        public void ItShouldCut()
        {
            var cutCommandMock = new Mock<ICutCommand>();
            _commandFactoryMock.Setup(f => f.CreateCutCommand())
                .Returns(() => cutCommandMock.Object);

            var sut = CreateSut();
            sut.Cut();
            cutCommandMock.Verify(d => d.Cut(sut.FullPath), Times.Once);
        }

        [Fact]
        public void ItShouldPaste()
        {
            var pastCommandMock = new Mock<IPastCommand>();
            _commandFactoryMock.Setup(f => f.CreatePastCommand())
                .Returns(() => pastCommandMock.Object);

            const string destionation = "C://dist";

            File.Paste(destionation);
             
            pastCommandMock.Verify(d => d.Past(destionation), Times.Once);
        }
    }
}
